package com.epam.edu.online.view;

@FunctionalInterface
public interface Printable {
    void print();
}
