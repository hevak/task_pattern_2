package com.epam.edu.online.view;

import com.epam.edu.online.model.controller.Controller;
import com.epam.edu.online.model.controller.ProductController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static final Logger log = LogManager.getLogger(MyView.class);

    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMainMenu = new LinkedHashMap<>();
    public static Scanner input = new Scanner(System.in);
    private Controller controller = new ProductController(1, "new Facebook");

    public MyView() {
        fillMenu();
        fillMethodMenu();
        printMenu();
    }
    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`" + key + "` - " + "\t" + value));
    }
    private void fillMenu() {
        menu.put("1", "show product backlog");
        menu.put("2", "do work");
        menu.put("q", "q");
    }
    private void fillMethodMenu() {
        methodMainMenu.put("1", ()-> controller.showBacklog());
        methodMainMenu.put("2", ()-> controller.doSprints());
        methodMainMenu.put("q", () -> System.out.println("by"));
    }
    public void run() {
        String command = "";
        do {
            try {
                command = input.next().toLowerCase();
                methodMainMenu.get(command).print();
            } catch (NullPointerException e) {
                log.error("wrongcommand");
                printMenu();
            }

        } while (!command.equalsIgnoreCase("Q"));
    }


}
