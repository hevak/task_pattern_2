package com.epam.edu.online;

import com.epam.edu.online.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().run();
    }
}
