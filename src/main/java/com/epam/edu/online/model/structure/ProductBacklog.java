package com.epam.edu.online.model.structure;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ProductBacklog {
    private static final Logger log = LogManager.getLogger(ProductBacklog.class);

    private int id;
    private String nameProduct;
    private List<Task> tasks;

    ProductBacklog(int id, String nameProduct) {
        this.id = id;
        this.nameProduct = nameProduct;
    }

    public int getId() {
        return id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void createAllTask(List<Task> allTask) {
        if (this.tasks == null) {
            this.tasks = allTask;
        } else {
            log.info("task already created");
        }
    }

    public void addTask(Task task){
        if (this.tasks == null) {
            log.info("first need create tasks");
        } else {
            this.tasks.add(task);
        }
    }
}
