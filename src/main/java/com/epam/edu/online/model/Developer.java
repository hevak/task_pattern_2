package com.epam.edu.online.model;

import com.epam.edu.online.Application;
import com.epam.edu.online.model.structure.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Developer extends User{
    private static final Logger log = LogManager.getLogger(Application.class);

    private Task task;

    public Developer(int id, String name) {
        super(id, name);
    }

    public void setTask(Task task) {
        task.setExecutor(this);
        log.info(this.getName()+" - task moved to 'todo'");
        task.setState(State.TO_DO);
        this.task = task;
    }

    public void doTask(Task task) {
        if (task.getState().equals(State.TO_DO)
                || task.getState().equals(State.TESTING)) {
            log.info(this.getName()+" - task moved to 'in progress'");
            task.setState(State.IN_PROGRESS);
            testTask(task);
        } else {
            log.info("bad previous state");
        }
    }

    public void testTask(Task task) {
        if (task.getState().equals(State.IN_PROGRESS)) {
            log.info(this.getName()+" - task moved to 'testing'");
            task.setState(State.TESTING);
            endTask(task);
        } else {
            log.info("bad previous state");
        }
    }

    public void endTask(Task task) {
        if (task.getState().equals(State.TESTING)) {
            if(new Random().nextInt(5)==1){
                task.getExecutor().doTask(task);
            } else {
                log.info(this.getName()+" - task moved to 'done'");
                task.setState(State.DONE);
            }

        } else {
            log.info("bad previous state");
        }
    }

    public Task getTask() {
        return task;
    }
}
