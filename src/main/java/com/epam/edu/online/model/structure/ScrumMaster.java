package com.epam.edu.online.model.structure;

import com.epam.edu.online.model.Developer;
import com.epam.edu.online.model.User;

import java.util.List;

public class ScrumMaster extends User {

    private ProductBacklog productBacklog;

    public ScrumMaster(int id, String name) {
        super(id, name);
    }

    public ProductBacklog createProductBacklog(int id, String nameProduct) {
        this.productBacklog = new ProductBacklog(id, nameProduct);
        return this.productBacklog;
    }
    public Sprint createSprint(List<Task> tasks) {
        return new Sprint(tasks);
    }
    public Task createTask(int id, String name, String description) {
        return new Task(id, name, description);
    }

    public ProductBacklog getProductBacklog() {
        return productBacklog;
    }
}
