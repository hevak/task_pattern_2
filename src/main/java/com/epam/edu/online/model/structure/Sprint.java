package com.epam.edu.online.model.structure;

import java.util.List;

public class Sprint {
    private List<Task> tasks;

    Sprint(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
