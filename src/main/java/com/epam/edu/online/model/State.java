package com.epam.edu.online.model;

public enum State {
    CREATED, TO_DO, IN_PROGRESS, TESTING, DONE
}
