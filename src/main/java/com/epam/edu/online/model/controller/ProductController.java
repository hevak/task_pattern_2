package com.epam.edu.online.model.controller;

import com.epam.edu.online.model.Developer;
import com.epam.edu.online.model.State;
import com.epam.edu.online.model.structure.ProductBacklog;
import com.epam.edu.online.model.structure.ScrumMaster;
import com.epam.edu.online.model.structure.Sprint;
import com.epam.edu.online.model.structure.Task;
import com.epam.edu.online.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class ProductController implements Controller {
    private static final Logger log = LogManager.getLogger(MyView.class);
    private final Sprint sprint1;
    private final Sprint sprint2;

    private ScrumMaster master = new ScrumMaster(3, "Orest");
    private Developer developer1 = new Developer(1, "Oleh");
    private Developer developer2 = new Developer(2, "Mykyta");

    public ProductController(int id, String nameProduct) {
        ProductBacklog productBacklog = master.createProductBacklog(id, nameProduct);

        fillProductBacklog(master, productBacklog);

        List<Task> tasks = master.getProductBacklog().getTasks().subList(0, 3);
        sprint1 = master.createSprint(tasks);

        List<Task> tasks1 = master.getProductBacklog().getTasks().subList(3, 6);
        sprint2 = master.createSprint(tasks1);

    }

    public void showBacklog() {
        master.getProductBacklog().getTasks()
                .stream()
                .filter(task -> !task.getState().equals(State.DONE))
                .forEach(log::info);

    }

    public void doSprints() {
        doSprints(sprint1);
        doSprints(sprint2);
    }

    private void doSprints(Sprint sprint) {
        doSprint(sprint);
    }





    private void doSprint(Sprint sprint1) {
        log.info("Beginning Sprint");
        int count = 1;
        for (Task task : sprint1.getTasks()) {
            if (count % 2 == 0) {
                doWork(developer1, task);
            } else {
                doWork(developer2, task);
            }
            count++;
        }
        log.info("Ending Sprint\n");
    }

    private void doWork(Developer developer, Task task) {
        task.setExecutor(developer);
        task.getExecutor().setTask(task);
        task.getExecutor().doTask(task);
    }

    private void fillProductBacklog(ScrumMaster master, ProductBacklog productBacklog) {
        Task task1 = master.createTask(1, "to do db", "need postgresql");
        Task task2 = master.createTask(2, "to do auth", "need realize oauth2");
        Task task3 = master.createTask(3, "to do pattern", "need realize state pattern");
        Task task4 = master.createTask(4, "to do right access", "need roles");
        Task task5 = master.createTask(5, "to do endpoints", "need rest");
        Task task6 = master.createTask(6, "to do error handling", "404");
        productBacklog.createAllTask(Arrays.asList(task1, task2, task3, task4, task5, task6));
    }
}
