package com.epam.edu.online.model.structure;

import com.epam.edu.online.model.Developer;
import com.epam.edu.online.model.State;
import com.epam.edu.online.model.User;

public class Task {

    private int id;
    private String name;
    private String description;
    private Developer executor;
    private State state;

    Task(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.state = State.CREATED;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Developer getExecutor() {
        return executor;
    }

    public void setExecutor(Developer executor) {
        this.executor = executor;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != task.id) return false;
        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        if (description != null ? !description.equals(task.description) : task.description != null) return false;
        if (executor != null ? !executor.equals(task.executor) : task.executor != null) return false;
        return state == task.state;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (executor != null ? executor.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", executor=" + executor +
                ", state=" + state +
                '}';
    }
}
