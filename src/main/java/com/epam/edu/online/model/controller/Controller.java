package com.epam.edu.online.model.controller;

public interface Controller {
    void showBacklog();
    void doSprints();
}
